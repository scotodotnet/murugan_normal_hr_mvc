﻿$(function () {

   

    $('.select2').select2();


});

function LoadEmployeeType() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/ReportPayroll/GetEmployeeType',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlEmpType').empty();
            $("#ddlEmpType").append($('<option></option>').val('0').text('--Select Employee Type--'));
            $.each(data, function (i, val) {

                $("#ddlEmpType").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadFinancialYear() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/ReportPayroll/FinancialYear1',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlFin').empty();
            $("#ddlFin").append($('<option></option>').val('0').text('-Select-'));
            $.each(data, function (i, val) {

                $("#ddlFin").append($('<option></option>').val(val.Name).text(val.Name));

            })
        }
    })
}

$(document).ready(function () {
    LoadEmployeeType();
    LoadFinancialYear();
    
});
$("#btnReport").click(function () {
    var FinYear = $('#ddlFin').val();
    var WagesType = $('#ddlEmpType').val();

    var isAllValid = true;


    if (isAllValid) {

        $.ajax({
            url: '/ReportPayroll/PaySlip',
            success: function () {
                var url = '/ReportPayroll/BonusDetails/?WagesType=' + WagesType + '&FinYear=' + FinYear + '';
                window.open(url);
            },
            error: function () {
                alert('Error. Please try again.');

            }
        });


    }
});