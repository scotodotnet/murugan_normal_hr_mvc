﻿$(function () {

    //alert('Test')
    $('.datepicker').datepicker(
        {
            format: "dd/mm/yyyy",
            autoclose: true
        });

    $('.select2').select2();
   

});

function LoadEmployeeType() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Others/GetEmployeeType',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlEmpType').empty();
            $("#ddlEmpType").append($('<option></option>').val('0').text('--Select Employee Type--'));
            $.each(data, function (i, val) {

                $("#ddlEmpType").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadFinancialYear() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Others/FinancialYear1',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlFin').empty();
            $("#ddlFin").append($('<option></option>').val('0').text('-Select-'));
            $.each(data, function (i, val) {

                $("#ddlFin").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

$(document).ready(function () {
    LoadEmployeeType();
    LoadFinancialYear();
   
});