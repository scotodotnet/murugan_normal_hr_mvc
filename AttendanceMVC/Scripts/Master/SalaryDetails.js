﻿$(function () {

    ////alert('Test')
    //$('.datepicker').datepicker(
    //    {
    //        format: "dd/mm/yyyy",
    //        autoclose: true
    //    });

    $('.select2').select2();
    $('#example1').DataTable();
   

});


function LoadEmployeeType() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Master/GetEmployeeType',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlEmpType').empty();
            $("#ddlEmpType").append($('<option></option>').val('0').text('--Select Employee Type--'));
            $.each(data, function (i, val) {

                $("#ddlEmpType").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function OnChangeEmpType() {
    //alert('Dept');
    var EmpType = $('#ddlEmpType').val();

    $.ajax({
        type: "GET",
        url: '/Master/GetEmpTypeDetails/?EmpType=' + EmpType + '',
        success: function (data) {
            //console.log(data);
            if (data.length != 0) {
                $('#txtBasic').val(data[0].BasicSalary);
                $('#txtConvey').val(data[0].Convey);
                $('#txtHRA').val(data[0].HRA);
                $('#txtWashingAllow').val(data[0].ExistingCode);
                $('#txtOthers').val(data[0].Machine_No);
            }
            else {
                $('#txtBasic').val('');
                $('#txtConvey').val('');
                $('#txtHRA').val('');
                $('#txtWashingAllow').val('');
                $('#txtOthers').val('');
            }
        }
    })

}

$(document).ready(function () {

    LoadEmployeeType();


    $('#SalaryDetSave').click(function () {

        //e.preventDefault();
        //alert('hi');
        //validation 
        var isAllValid = true;

        if ($('#ddlEmpType').val() == '0') {
            document.getElementById("EmpTypeerror").innerHTML = "Select the Employee Type";
            isAllValid = false;
           
        }
        else {

            document.getElementById("EmpTypeerror").innerHTML = " "; // remove it
        }

        if ($('#txtBasic').val() == '') {
            document.getElementById("Basicerror").innerHTML = "Enter the Basic";
            isAllValid = false;
            
        }
        else {

            document.getElementById("Basicerror").innerHTML = " "; // remove it
        }

        if ($('#txtConvey').val() == '') {
            document.getElementById("Conveyerror").innerHTML = "Enter the Convey";
            isAllValid = false;
           
        }
        else {

            document.getElementById("Conveyerror").innerHTML = " "; // remove it
        }

        if ($('#txtHRA').val() == '') {
            document.getElementById("HRAerror").innerHTML = "Enter the HRA";
            isAllValid = false;
           
        }
        else {

            document.getElementById("HRAerror").innerHTML = " "; // remove it
        }
        if ($('#txtWashingAllow').val() == '') {
            document.getElementById("WashingAllowerror").innerHTML = "Enter the WashingAllowance";
            isAllValid = false;

        }
        else {

            document.getElementById("WashingAllowerror").innerHTML = " "; // remove it
        }


        if ($('#txtOthers').val() == '') {
            document.getElementById("Otherserror").innerHTML = "Enter the Others";
            isAllValid = false;

        }
        else {

            document.getElementById("Otherserror").innerHTML = " "; // remove it
        }


        //Save if valid
        if (isAllValid) {

            //alert('hi');

            var RoomDet = {
                UserType: $('#ddlEmpType').val(),
                BasicSalary: $('#txtBasic').val().trim(),
                Convey: $('#txtConvey').val().trim(),
                HRA: $('#txtHRA').val().trim(),
                ExistingCode: $('#txtWashingAllow').val().trim(),
                Machine_No: $('#txtOthers').val().trim(),

            }
            console.log(RoomDet);
            $.ajax({
                url: "/Master/SaveSalaryDetails",
                type: "POST",
                data: JSON.stringify(RoomDet),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d == "Insert") {
                        clear1();
                        alert('Inserted Successfully..');

                    }
                    else if (d == "Update") {

                        clear1();
                        alert('Update Successfully...');

                    }

                },
                error: function () {
                    alert('Error. Please try again.');
                }

            });
        }


    });

    $('#SalaryDetClear').click(function () {
        clear1();
    });

});

function clear1() {
    $("#ddlEmpType").val('0').change();
    $("#txtBasic").val('0');
    $("#txtConvey").val('0');
    $("#txtHRA").val('0');

    document.getElementById("EmpTypeerror").innerHTML = " ";
    document.getElementById("Basicerror").innerHTML = " ";
    document.getElementById("Conveyerror").innerHTML = " ";
    document.getElementById("HRAerror").innerHTML = " ";
}