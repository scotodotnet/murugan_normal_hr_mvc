﻿$(function () {

    ////alert('Test')
    //$('.datepicker').datepicker(
    //    {
    //        format: "dd/mm/yyyy",
    //        autoclose: true
    //    });

    $('.select2').select2();
    $('#example1').DataTable();


});


function LoadEmployeeType() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Master/GetEmployeeType',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlEmpType').empty();
            $("#ddlEmpType").append($('<option></option>').val('0').text('--Select Employee Type--'));
            $.each(data, function (i, val) {

                $("#ddlEmpType").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function OnChangeINEmpType() {
    //alert('Dept');
    var EmpType = $('#ddlEmpType').val();

    $.ajax({
        type: "GET",
        url: '/Master/GetEmpTypeIncentive/?EmpType=' + EmpType + '',
        success: function (data) {
            //console.log(data);

            if (data.length != 0) {
                $('#txtDays').val(data[0].Days);
                $('#txtAmount').val(data[0].Amount);

            }
            else {
                $('#txtDays').val('');
                $('#txtAmount').val('');

            }
           
        }
    })

}

$(document).ready(function () {

    LoadEmployeeType();


    $('#btnIncentiveSave').click(function () {

        //alert('hi');
        //validation 
        var isAllValid = true;

        if ($('#ddlEmpType').val() == '0') {
            document.getElementById("EmpTypeerror").innerHTML = "Select the Employee Type";
            isAllValid = false;
        }
        else {

            document.getElementById("EmpTypeerror").innerHTML = " "; // remove it
        }

        if ($('#txtDays').val() == '') {
            document.getElementById("Dayserror").innerHTML = "Enter the Days";
            isAllValid = false;
        }
        else {

            document.getElementById("Dayserror").innerHTML = " "; // remove it
        }

        if ($('#txtAmount').val() == '') {
            document.getElementById("Amounterror").innerHTML = "Enter the Amount";
            isAllValid = false;
        }
        else {

            document.getElementById("Amounterror").innerHTML = " "; // remove it
        }

       

        //Save if valid
        if (isAllValid) {
            var RoomDet = {
                UserType: $('#ddlEmpType').val(),
                Days: $('#txtDays').val().trim(),
                Amount: $('#txtAmount').val().trim(),
               


            }
            console.log(RoomDet);
            $.ajax({
                url: "/Master/SaveIncentiveDetails",
                type: "POST",
                data: JSON.stringify(RoomDet),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    //alert(d);
                    if (d == "Insert") {
                       
                        alert('Inserted Successfully..');
                         clear1();

                    }
                    else if (d == "Update") {

                       
                        alert('Update Successfully...');
                         clear1();

                    }

                },
                error: function () {
                    alert('Error. Please try again.');
                }

            });
        }


    });


    $('#btnIncentiveClear').click(function () {
        clear1();
    });

});

function clear1() {
    $("#ddlEmpType").val('0').change();
    $("#txtDays").val('0');
    $("#txtAmount").val('0');


    document.getElementById("EmpTypeerror").innerHTML = " ";
    document.getElementById("Dayserror").innerHTML = " ";
    document.getElementById("Amounterror").innerHTML = " ";
}