﻿$(function () {

    //alert('Test')
    $('.datepicker').datepicker(
        {
            format: "dd/mm/yyyy",
            autoclose: true
        });

    $('.select2').select2();


});

function LoadEmployeeType() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Employee/GetEmployeeType',
        success: function (data) {
            //console.log(data); ddlAttendEmpType
            //Employee = data;
            $('#ddlSalaryEmpType').empty();
            $('#ddlAttendEmpType').empty();
            $("#ddlSalaryEmpType").append($('<option></option>').val('0').text('--Select Employee Type--'));
            $("#ddlAttendEmpType").append($('<option></option>').val('0').text('--Select Employee Type--'));

            $.each(data, function (i, val) {

                $("#ddlSalaryEmpType").append($('<option></option>').val(val.ID).text(val.Name));
                $("#ddlAttendEmpType").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

$(document).ready(function () {
    LoadEmployeeType();


    $("#btnSalaryDownload").click(function () {


        var WagesType = $('#ddlSalaryEmpType').val();
        var FromDate = $('#txtFromDate').val();

        var isAllValid = true;
        
        if ($('#ddlSalaryEmpType').val() == '0') {

            document.getElementById("SalaryEmpTypeerror").innerHTML = "Select The Wages Type";

            isAllValid = false;
        }
        else {

            document.getElementById("SalaryEmpTypeerror").innerHTML = " ";  // remove it
        }

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the Attendance Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }


        if (isAllValid)
        {

            $.ajax({
                url: '/Others/SalaryDownload',
                success: function () {
                    var url = '/Others/GetSalaryDownload/?WagesType=' + WagesType + '&AttendanceDate=' + FromDate + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });           
        }

    });

    $("#btnAttendDownload").click(function () {

        var WagesType = $('#ddlAttendEmpType').val();
        var FromDate = $('#txtFromDate').val();
        //alert(WagesType);

        var isAllValid = true;

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the Attendance Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

       

        if ($('#ddlAttendEmpType').val() == '0') {

            document.getElementById("AttendEmpTypeerror").innerHTML = "Select The Wages Type";

            isAllValid = false;
        }
        else {

            document.getElementById("AttendEmpTypeerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {
            $.ajax({
                url: '/Others/AttendanceDownload',
                success: function () {
                    var url = '/Others/GetAttendanceDownload/?WagesType=' + WagesType + '&AttendanceDate=' + FromDate + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });
        }

    });
   
});
