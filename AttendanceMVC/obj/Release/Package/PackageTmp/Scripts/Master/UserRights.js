﻿$(function () {
    $('#Userpageexample').DataTable();
    $('.select2').select2();
    $('.datepicker').datepicker(
       {
           format: "dd/mm/yyyy",
           autoclose: true
       });
});



function loadUserName() {

    $.ajax({
        type: "GET",
        url: '/Master/GetUserName',
        success: function (data) {
            if (data.length != 0) {
                $('#ddlUserName').empty();
                $("#ddlUserName").append($('<option></option>').val('0').text('Select'));
                $.each(data, function (i, val) {

                    $("#ddlUserName").append($('<option></option>').val(val.UserName).text(val.UserName));

                })
            }
            else {
                $('#ddlUserName').empty();
                $("#ddlUserName").append($('<option></option>').val('0').text('Select'));
            }


        },
        error: function () {
            alert('Error. Please try again.');

        }
    });
}


function LoadDatatable() {

    var ModuleName = $('#ddlModuleName').val();
    var MenuName = $('#ddlMenuName').val();
    var UserName = $('#ddlUserName').val();

    var isAllValid = true;



    if ($('#ddlUserName').val() == '0') {

        document.getElementById("UserNameerror").innerHTML = "Select the UserName";

        isAllValid = false;
    }
    else {

        document.getElementById("UserNameerror").innerHTML = " ";  // remove it
    }


    if ($('#ddlModuleName').val() == '0') {

        document.getElementById("ModuleNameerror").innerHTML = "Select the ModuleName";
        isAllValid = false;
    }
    else {

        document.getElementById("ModuleNameerror").innerHTML = " "; // remove it
    }

    if ($('#ddlMenuName').val() == '0') {

        document.getElementById("MenuNameerror").innerHTML = "Select the MenuName";

        isAllValid = false;
    }
    else {

        document.getElementById("MenuNameerror").innerHTML = " ";  // remove it
    }

    if (isAllValid) {
        $.ajax({
            type: "GET",
            url: '/Master/OnChanageMenuNameOne/?ModuleName=' + ModuleName + '&MenuName=' + MenuName + '&UserName=' + UserName + '',
            success: function (data) {
                if (data.length != 0) {

                    $("#example1").find('tbody').empty();

                    var i = 1;
                    $.each(data, function (key, item) {

                        if (item.View == "0") {
                            View = "";
                        }
                        else {
                            View = "checked";
                        }

                        $("#example1").find('tbody')
                        .append(
                        '<tr><td>' + item.FormName + '</td> <td> <input type="checkbox" id="checkboxes' + i + '" ' + View + '/> </td> </tr>'
                    );
                        i++;
                    });

                }
                else {

                    $('#example1').empty();

                }


            },
            error: function () {
                alert('Error. Please try again.');

            }
        });
    }  


}


function LoadModulaName() {

    $.ajax({
        type: "GET",
        url: '/Master/ModulaName',
        success: function (data) {
            if (data.length != 0) {
                $('#ddlModuleName').empty();
                $("#ddlModuleName").append($('<option></option>').val('0').text('Select'));
                $.each(data, function (i, val) {

                    $("#ddlModuleName").append($('<option></option>').val(val.ModuleName).text(val.ModuleName));

                })
            }
            else {
                $('#ddlModuleName').empty();
                $("#ddlModuleName").append($('<option></option>').val('0').text('Select'));
            }


        },
        error: function () {
            alert('Error. Please try again.');

        }
    });
}

function OnChangeMenu() {

    var ModuleName = $('#ddlModuleName').val();
    //var FormName = $('#ddlMenuName').val();
   
    if (ModuleName != 0) {
        $.ajax({
            type: "GET",
            url: '/Master/OnChanageModuleName/?ModuleName=' + ModuleName + '',
            success: function (data) {
                if (data.length != 0) {
                    $('#ddlMenuName').empty();
                    $("#ddlMenuName").append($('<option></option>').val('0').text('Select'));
                    $.each(data, function (i, val) {
                      
                        $("#ddlMenuName").append($('<option></option>').val(val.FormName).text(val.FormName));

                    })
                }
                else {
                    $('#ddlMenuName').empty();
                    $("#ddlMenuName").append($('<option></option>').val('0').text('Select'));
                }

            },
            error: function () {
                alert('Error. Please try again.');

            }
        });
    }
    else {
        //alert('Please Select Module Name and Menu Name');
        $('#ddlMenuName').empty();
        $("#ddlMenuName").append($('<option></option>').val('0').text('Select'));
    }
}

$(document).ready(function () {

    loadUserName();
    LoadModulaName();
   
    $('#btnSave').click(function () {

        var name = $('#ddlUserName').val();
        var isAllValid = true;
        var TableData = [];


        if ($('#ddlUserName').val() == '0') {

            document.getElementById("UserNameerror").innerHTML = "Select the UserName";

            isAllValid = false;
        }
        else {

            document.getElementById("UserNameerror").innerHTML = " ";  // remove it
        }


        if ($('#ddlModuleName').val() == '0') {

            document.getElementById("ModuleNameerror").innerHTML = "Select the ModuleName";
            isAllValid = false;
        }
        else {

            document.getElementById("ModuleNameerror").innerHTML = " "; // remove it
        }

        if ($('#ddlMenuName').val() == '0') {

            document.getElementById("MenuNameerror").innerHTML = "Select the MenuName";

            isAllValid = false;
        }
        else {

            document.getElementById("MenuNameerror").innerHTML = " ";  // remove it
        }




        var j = 1;
        //Put the table value in list
        $('#example1 tbody').find('tr').each(function (row, tr) {

            var $tds = $(this).find('td');
            var tt = $('#checkboxes' + j + '').prop('checked') ? 1 : 0;

            //if(tt == "1")
            //{
            TableData.push({

                FormName: $tds.eq(0).text(),             
                View: tt,



            });
            //}

            j++;

        });

        if (TableData.length == 0) {
            alert('Add atleast one Item Details');

            isAllValid = false;
        }




        if (isAllValid) {

            var RoomDet = {
                UserName: $('#ddlUserName').val(),
                ModuleName: $('#ddlModuleName').val(),
                MenuName: $('#ddlMenuName').val(),
                GetItemDet: TableData
            }

            console.log(RoomDet);

            $.ajax({

                url: "/Master/SaveUserRight",
                type: "POST",
                data: JSON.stringify(RoomDet),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d == "Insert") {
                        alert('Inserted Successfully..');
                        clear();
                    }
                    else if (d == "Update") {
                        alert('Update Successfully...');
                        clear();
                    }
                    else if (d == "No Row") {
                        alert('No Row Inserted ...');
                        clear();
                    }

                },
                error: function () {
                    alert('Error. Please try again.');
                }

            });

        }
    });

    $('#btnClear').click(function () {

        clear();

    });
   
});


function clear() {

    document.getElementById("UserNameerror").innerHTML = " ";
    document.getElementById("ModuleNameerror").innerHTML = " "; // remove it
    document.getElementById("MenuNameerror").innerHTML = " ";  // remove it
    $('#ddlUserName').val('0').change();
    $('#ddlModuleName').val('0').change();
    $('#ddlMenuName').val('0').change();

    $("#example1").find('tbody').empty();

    if ($('#ddlMenuName').val() == '0') {
        document.getElementById("UserNameerror").innerHTML = " ";
        document.getElementById("ModuleNameerror").innerHTML = " "; // remove it
        document.getElementById("MenuNameerror").innerHTML = " ";  // remove it
    }
}



