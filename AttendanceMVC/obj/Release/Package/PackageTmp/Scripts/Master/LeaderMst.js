﻿$(function () {

    //alert('Test')
    $('.datepicker').datepicker(
        {
            format: "dd/mm/yyyy",
            autoclose: true
        });

    $('.select2').select2();
    $('#example1').DataTable();
   

});

function LoadDesignation() {
    //alert('Dept');


    //alert(DeptID);
    $.ajax({
        type: "GET",
        url: '/Master/GetDesignation',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlDesignation').empty();
            $("#ddlDesignation").append($('<option></option>').val('0').text('--Select Designation--'));
            $.each(data, function (i, val) {

                $("#ddlDesignation").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })

}


function LoadDept() {
   


    //alert(DeptID);
    $.ajax({
        type: "GET",
        url: '/Master/GetDept',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlDept').empty();
            $("#ddlDept").append($('<option></option>').val('0').text('--Select Dept--'));
            $.each(data, function (i, val) {

                $("#ddlDept").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })

}


function LoadDeptChanged() {

    var Dept = $("#ddlDept").val();
    
    $.ajax({
        type: "GET",
        url: '/Master/GetDesignation_Leader/?Dept=' + Dept + '',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlDesignation').empty();
            $("#ddlDesignation").append($('<option></option>').val('0').text('--Select Designation--'));
            $.each(data, function (i, val) {

                $("#ddlDesignation").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })

}
function LoadLeaderNames() {
   
   // var Leader = $("#ddlLeaderName").val();
    $.ajax({
        type: "GET",
        url: '/Master/GetLeader',
        success: function (data) {

            $('#ddlLeaderName').empty();
            $("#ddlLeaderName").append($('<option></option>').val('0').text('--Select--'));
            $.each(data, function (i, val) {

                $("#ddlLeaderName").append($('<option></option>').val(val.ID).text(val.Name));
            })
        }
    })
}
$(document).ready(function () {
    LoadLeaderAllotmentDetails();
    //LoadDesignation();   
    LoadDept();
    LoadLeaderNames();
});

$('#btnAdd').click(function () {

    $('#LeaderMain').css('display', 'none');

    $('#LeaderSub').css('display', 'block');

});


$('#BtnSave_Leader').click(function()
{

    var isAllValid = true;

    //if ($('#ddlDesignation').val() == '0') {
    //    document.getElementById("Designationerror").innerHTML = "Select the Designation";
    //    isAllValid = false;
    //}
    //else {

    //    document.getElementById("Designationerror").innerHTML = " "; // remove it
    //}
    //if ($('#ddlDept').val() == '0') {
    //    document.getElementById("Depterror").innerHTML = "Select the Dept";
    //    isAllValid = false;
    //}
    //else {

    //    document.getElementById("Depterror").innerHTML = " "; // remove it
    //}



    //Save if valid
    if (isAllValid) {
       
        var RoomDet = {
            //ID: $('#ddlDesignation').val(),
            AgentID: $('#ddlLeaderName').val().trim(),           
            //DeptCode: $('#ddlDept').val().trim(),
            Total: $('#txtTotal').val().trim(),

        }
        console.log(RoomDet);
        $.ajax({
            url: "/Master/InsertLeader",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    //LoadDesignation();
                    //LoadDept();
                    LoadLeaderAllotmentDetails();
                    LoadLeaderNames();
                    clear();
                    alert('Inserted Successfully..');
                    $('#LabourSub').css('display', 'none');
                    $('#LabourMain').css('display', 'block');

                }
                else if (d == "Update") {
                    //LoadDesignation();
                    //LoadDept();
                    LoadLeaderAllotmentDetails();
                    LoadLeaderNames();
                    clear();
                    alert('Update Successfully...');
                    $('#LabourSub').css('display', 'none');
                    $('#LabourMain').css('display', 'block');

                }

            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

/*$('#BtnSave').click(function () {

   
    //validation 
    var isAllValid = true;

    if ($('#ddlDesignation').val() == '0') {
        document.getElementById("Designationerror").innerHTML = "Select the Designation";
        isAllValid = false;
    }
    else {

        document.getElementById("Designationerror").innerHTML = " "; // remove it
    }
    if ($('#ddlDept').val() == '0') {
        document.getElementById("Depterror").innerHTML = "Select the Dept";
        isAllValid = false;
    }
    else {

        document.getElementById("Depterror").innerHTML = " "; // remove it
    }



    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            ID: $('#ddlDesignation').val(),
            Name: $('#txtWorkercode').val().trim(),
            Category: $('#txtGeneral').val().trim(),
            Shift: $('#txtSHIFT1').val().trim(),
            SubCatName: $('#txtSHIFT2').val().trim(),
            MachineID: $('#txtSHIFT3').val().trim(),
            LastName: $('#txtTotal').val().trim(),

        }
        console.log(RoomDet);
        $.ajax({
            url: "/Master/InsertLabourAllotment",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadDesignation();
                    LoadLabourAllotmentDetails();
                    clear();
                    alert('Inserted Successfully..');
                    $('#LabourSub').css('display', 'none');
                    $('#LabourMain').css('display', 'block');

                }
                else if (d == "Update") {
                    LoadDesignation();
                    LoadLabourAllotmentDetails();
                    clear();
                    alert('Update Successfully...');
                    $('#LabourSub').css('display', 'none');
                    $('#LabourMain').css('display', 'block');

                }

            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});*/

$('#BtnClear').click(function () {
    clear();
});

function clear() {
    LoadLeaderAllotmentDetails();
    LoadLeaderNames();
    $("#txtTotal").val('0');
}

function LoadLeaderAllotmentDetails() {

    $.ajax({
        type: "GET",
        url: '/Master/GetLeaderAllotment',
        success: function (data) {
            var data1 = $('#example8').DataTable();
            data1.clear();
            if (data.length != 0) {
                $.each(data, function (key, item) {
                    data1.row.add([
                    item.Name,
                    item.Category,
                    "<button class='btn btn-success' type='button' onClick='EditLeaderAllotment(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteLeaderAllotment(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                    ]).draw();
                });
            }
            else {
                var data1 = $('#example8').DataTable();
                data1.clear();
            }

        }
    });
}

function EditLeaderAllotment(index) {

    $.ajax({
        type: "GET",
        url: '/Master/GetEditLeaderAllotment/?ID=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;


                $('#LeaderMain').css('display', 'none');

                $('#LeaderSub').css('display', 'block');

                $("#ddlLeaderName").val(item.ID).change();
                $("#txtTotal").val(item.Category);
               

            });

        }
    });

}

function DeleteLeaderAllotment(index) {

    $.ajax({
        type: "GET",
        url: '/Master/GetDeleteLeaderAllotment/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadLeaderAllotmentDetails();
                LoadLeaderNames();
                clear();

                $('#LeaderMain').css('display', 'block');

                $('#LeaderSub').css('display', 'none');
                alert('Deleted Successfully..');

            }
            else if (data == "No Data") {
                LoadLeaderAllotmentDetails();
                LoadLeaderNames();
                clear();

                $('#LeaderMain').css('display', 'block');

                $('#LeaderSub').css('display', 'none');
                alert('No Data...');
            }
        }

    });
}